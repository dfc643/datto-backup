#!/bin/bash
TITLE="MoeArt's Disk Backup Utilty"

#--------------------
#      WELCOME
#--------------------
i18n_welcome="Welcome to $TITLE\n\n
This is a CoW (Copy on Write) disk backup utility based on dattobd, You can dumping disk image from current live system disk, and keep the integrity of the file as far as possible!\n\n
Any disk related operation must be cautious! Please don't think that CoW is very safe. Please use this tool in a clear mind. The author is not responsible for the loss of disk data!\n\n
Copyright (c)2021 xRetia Labs\n
Copyright (c)2021 MoeArt Inc. - www.acgdraw.com\n
Version   1.1b20201231"


#--------------------
#     CHECK DEPS
#--------------------
i18n_dattobd_missing="dattobd driver is unload, Please check is dattobd installed?\n\n
Installation Guide: https://github.com/datto/dattobd/blob/master/INSTALL.md"

i18n_deps_missing="Dependent packages 'pv, pigz' is not installed!\n\n
Installation Guide: sudo apt-get install -y pv pigz"


#--------------------
#   DISK SELECTION
#--------------------
i18n_unmount="[UNMOUNT]"
i18n_select_disk="Which disk you want backup?"


#--------------------
#   COMPRESS LEVEL
#--------------------
i18n_recommend="Recommend"
i18n_complevel="Level"
i18n_comp_title="Select one Compress Level (higher level = smaller size)"


#--------------------
#   SAVE LOCATION
#--------------------
i18n_save_title="Select save location"
i18n_save_lock="Location: %s\nThe current save location has been used, please change other save location, Byebye!"


#--------------------
#   USER CONFIRM
#--------------------
i18n_confirm_title="PLEASE THINK TWICE"
i18n_confirm_msg="The current operation will create a disk snapshot, and the data before and after creating the snapshot will be temporarily separated. During this period, the program can still continue to run and write data to the disk. \n\n
After the disk image is generated, the modification made by the running program (e.g. MySQL) to the disk data will be merged into the physical disk. \n\n
Generally, it will not damge the data, it is strongly recommended that you stop the running programs which writting data to the disk, such as MySQL."


#--------------------
#   CHECK SNAPDEV
#--------------------
i18n_snapdevlock_title="Snapshot device is in use"
i18n_snapdevlock_msg="The snapshot device %s is in use! If the device is not create by you, You can select YES to force unmount the snapshot device.\n\n
Please make sure the snapshop device is not in use before unmount the snapshot device (e.g. You are running another MADBU instance) \
Force unmount the snapshot device will cause DATA LOSSING If another process is in use!"



#--------------------
#      COMMON
#--------------------
i18n_error_title="ERROR OCCURED"
i18n_need_root="Need superuser (root) permissions!"
i18n_user_cancel="Cancelled"
i18n_user_cancel2="User Cancelled the Operation, Byebye!"
i18n_preparing="Preparing Working Environment ... "
i18n_creating_snap="Creating Disk Snapshot ... "
i18n_snapping_failed="Snapshot create failed, For Detail:"
i18n_backuping="Backuping Disk %s to %s ..."
i18n_merging="Merging Disk Snapshot ... "
i18n_finished="Backup Finished"
i18n_finished_msg="Disk backup finished, Save Location:"
i18n_backup_failed="Disk backup finished, BUT ERROR OCCURED when merging disk snapshot\n\n
Save Location: %s
Error Log: %s"

export LANGUAGE="en_US"
