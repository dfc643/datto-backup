# madbu (MoeArt's Disk Backup Utility)

A Text based UI to dump live (running) system disk to image file with Volume Shadow Service (VSS or COW) in Linux. need dattobd dkms module support.

![sample.gif](https://gitlab.com/dfc643/datto-backup/-/raw/master/sample.gif)

[![asciicast](https://asciinema.org/a/382307.svg)](https://asciinema.org/a/382307)

---

## Requirement
* CoW DKMS Module ```dattobd```
* Linux Kernel ```>=4.9``` or ```>5.0``` recommend
* Parallel GZip program ```pigz```
* Base packages ```dialog, pv, dd```


## Task
* [x] Volume Shadow Copy (CoW)
* [x] Umount Device Support
* [x] English Version
* [ ] Compression Algorithm Optimization
* [ ] Graphical User Interface (?)


## Before Use
1. Please install those packages before use.
    ```bash
    sudo apt install -y dialog pv pigz
    ```
1. And then install ```dattabd``` module.    
**For Debian and Ubuntu**
    ```
    sudo apt-key adv --fetch-keys https://cpkg.datto.com/DATTO-PKGS-GPG-KEY
    echo "deb [arch=amd64] https://cpkg.datto.com/datto-deb/public/$(lsb_release -sc) $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/datto-linux-agent.list
    sudo apt-get update
    sudo apt-get install dattobd-dkms dattobd-utils
    ```
    **For Deepin v20 and UOS v20**
    ```
    sudo apt-key adv --fetch-keys https://cpkg.datto.com/DATTO-PKGS-GPG-KEY
    echo "deb [arch=amd64] https://cpkg.datto.com/datto-deb/public/buster buster main" | sudo tee /etc/apt/sources.list.d/datto-linux-agent.list
    sudo apt-get update
    sudo apt-get install dattobd-dkms dattobd-utils
    ```
3. Reboot your PC or Server.


## Usage
### Backup Live System
```bash
git clone https://gitlab.com/dfc643/datto-backup
cd datto-backup/
chmod +x madbu.sh
sudo bash madbu.sh
```
### Restore from LiveCD or Another OS
Example in Linux LiveCD
```
gzip -dc /path/to/your/sda3.img.gz | dd of=/dev/sda3
```

# Copyright
## License
[The MIT License (MIT)](https://gitlab.com/dfc643/datto-backup/-/raw/master/LICENSE)

Copyright (c) 2021 xRetia Labs    
Copyright (c) 2021 MoeArt Inc. (www.acgdraw.com)

## About xRetia Labs
The xRetia Labs is apart of MoeArt Inc. which working for opensource project.    
Copyright (c) 2021 MoeArt Inc. (www.acgdraw.com)

---


# License for dattobd
### dattobd License Terms

dattobd is a kernel module for taking block-level snapshots and incremental backups of Linux block devices

Copyright © 2015 Datto, Inc.

### dattobd kernel module and userspace applications

dattobd is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, under version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### libdattobd userspace library

libdattobd is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

